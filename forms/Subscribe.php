<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Chimpmail
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * Subscribe
 *
 * the subscription Form
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Chimpmail
 */
class Chimpmail_Form_Subscribe extends Zend_Form
{
    /**
     * init
     *
     * Initializes the form
     * */
    public function init()
    {
        // set the default method
        $this->setMethod('post');
        $this->setName('Subscribe');
        $this->setAttrib('class', 'subscribe_quick');

        //list id
        $this->addElement($this->createElement('hidden', 'list_id',array(
            'size'=>100,
        )));
        $this->addElement($this->createElement('hidden', 'web_id',array(
            'size'=>100,
        )));
        
        //Email
        $this->addElement($this->createElement('text', 'email_address',array(
            'label'=>'E-mail:',
            'required'=>true,
            'onClick'=>'this.focus(); this.select();',
        ))->addValidators(array(
                array(
                    'validator'=>'notEmpty',
                    'breakChainOnFailure'=>true,
                ),
                array(
                    'validator'=>'stringLength',
                    'options'=>array(5,200),
                ),
                array(
                    'validator'=>'emailAddress',
                ),
            )));

        //submit
         $this->addElement($this->createElement('submit','Save',array(
                'label'=>'Save',
         )));
    }
}

