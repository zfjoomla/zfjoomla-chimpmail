<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Chimpmail
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * Company Banner
 *
 * the Banner form
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Chimpmail
 */
class Chimpmail_Form_Config extends Zend_Form
{
    /**
     * init
     *
     * Initializes the form
     * */
    public function init()
    {
        // set the default method
        $this->setMethod('post');
        $this->setName('Config');

        //apiKey
        $this->addElement($this->createElement('text', 'apiKey',array(
            'label'=>'Api Key:',
            'requred'=>true,
            'size'=>100,
        )));
        //apiUrl
        $this->addElement($this->createElement('text', 'apiUrl',array(
            'label'=>'Api Url:',
            'requred'=>true,
            'size'=>100,
        )));
        //sourceEmail
        $this->addElement($this->createElement('text', 'sourceEmail',array(
            'label'=>'Source Email:',
            'requred'=>true,
            'size'=>100,
        )));

        //submit
         $this->addElement($this->createElement('submit','Save',array(
                'label'=>'Save',
                'class'=>'ui-button ui-state-default ui-corner-all',
         )));

         //cancel
         $this->addElement($this->createElement('submit','Cancel',array(
                'label'=>'Cancel',
                'class'=>'ui-button ui-state-default ui-corner-all',
         )));
    }
}

