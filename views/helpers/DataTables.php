<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * Modules Zend_View_Helper_DataTables
 *
 * builds a carousel from jquery 
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend
 * @link http://www.thomaslanciauz.pro/jquery/jquery_carousel.html
 */
class Zend_View_Helper_DataTables extends Zend_View_Helper_Abstract{
    /**
     * DataTables
     *
     * enables a datatables
     *
     * @param string $id
     * @param array $args 
     * @return string
     */
    public function DataTables($id, array $args=array())
    {
        $jquery = $this->view->jQuery();
        $jquery->enable();
        $jqHandle = ZendX_JQuery_View_Helper_JQuery::getJQueryHandler();
        $arguments = array(
            'bPaginate'=>true,
            "bJQueryUI"=>true,
            "sPaginationType"=> "full_numbers",
        );
        $arguments = array_merge($arguments, $args);
        // ask jquery to run the plugin when it has loaded
        ob_start();?>
            <?php echo $jqHandle;?>.getScript('<?php
                echo APPLICATION_URL."/modules/Chimpmail/js/jquery.datatables.min.js";?>',
                function() {
                    <?php echo $jqHandle;?>('<?php echo $id;?>').dataTable(<?php echo sizeof($arguments)>0?json_encode($arguments):'';?>);
                });
            
        <?php
        $js = Core_View_Compress::compressJS(ob_get_clean());
        $jquery->addOnload($js);
        return '';
    }
}
