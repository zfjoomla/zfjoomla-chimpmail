<?php
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');

class Chimpmail_Bootstrap extends Zend_Application_Module_Bootstrap {
    /**
     * @var string the module name
     */
    protected $_name = "/Chimpmail";
    /**
     * _initAutoload()
     *
     * Initialize the autoloader and return to bootstrap
     *
     * @return mixed
     */
    protected function _initAutoload() 
    {
        // Add autoloader empty namespace
        $autoLoader = Zend_Loader_Autoloader::getInstance();
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
           'basePath'=>APPLICATION_PATH."/modules".$this->_name,
            'namespace'=>'',
            'resourceTypes'=>array(
              'form'=>array(
                  'path'=>'forms/',
                  'namespace'=>'Form_',
              ),
              'chimp'=>array(
                  'path'=>'library/Chimp/',
                  'namespace'=>'Chimp_',
              ),
            ),
        ));
        // Return it so that it can be stored by the bootstrap
        return $autoLoader;
    }
    /**
     * _initConfig
     * 
     * loads the configuration information from the databse
     */
    public function _initConfig() 
    {
        // create the models
        $mdlModules = new Model_ZFModules();
        $module = $mdlModules->getModuleByName($this->_name);
        if($module == NULL) {
            throw new Exception('Error, could not find module');
        }
        $params = json_decode($module->params);
        //apikey
        if(isset($params->apiKey)) {
            Zend_Registry::set('chimpApiKey',$params->apiKey);
        }
        //apiURL
        if(isset($params->apiUrl)) {
            Zend_Registry::set('chimpApiUrl',$params->apiUrl);
        }
        //sourceEmail
        if(isset($params->sourceEmail)) {
            Zend_Registry::set('chimpSourceEmail',$params->sourceEmail);
        }
    }
    /**
     * _iniStyle
     * 
     * adds a stylesheet to all items in this modulee
     */
    protected function _initStyle()
    {
        if (Zend_Registry::isRegistered('view')) {
            $view = Zend_Registry::get('view');
        } else {
            $view = new Zend_View();
            Zend_Registry::set('view',$view);
        }
        $view->headLink()->appendStylesheet(APPLICATION_URL.'/modules'.$this->_name.'/css/style.css');
    }
}