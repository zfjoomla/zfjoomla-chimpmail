<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend-Chimpmail
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * Modules Banners
 *
 * manages communication to the model for manageing zend based modules
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend-Chimpmail
 */
class Chimpmail_Model_Lists extends Zend_Db_Table_Abstract
{
    /**
     * @var string Acceptance name of the table within the database
     */
    protected $_name = 'zf_chimpmail_lists';
    /**
     * @var string name of the database to connect to
     */
    protected $_use_adapter = "joomla";
     /**
     * __construct
     *
     * queries the zend registry to select the proper database
     *
     * @param mixed $config
     * @return mixed
     */
    public function  __construct($config = null)
    {
        $this->_name = Zend_Registry::get('dbprefix') . $this->_name;
        if (isset($this->_use_adapter)) {
            $dbAdapters = Zend_Registry::get('dbAdapters');
            $config = ($dbAdapters[$this->_use_adapter]);
        }
        return parent::__construct($config);
    }
    /**
     * getName 
     * 
     * gets the name of the database table
     * 
     * @return string table name
     */
    public static function getName() {
        $mdl = new self();
        return $mdl->_name;
    }
    /**
     * getList
     *
     * gets a record from the database
     *
     * @param int $id the unique id for the databse
     * @return Zend_db_Table_Row|NULL
     */
    public function getList($id)
    {
        $mdl = $this;
        $resultSet = $mdl->find($id);
        if($resultSet->count() >0) {
            return $resultSet->current();
        }
        return NULL;
    }
    /**
     * getChimpList
     * 
     * searches the database to find a record by its chimp mail id
     * 
     * @param string $id
     * @return Zend_Db_Table_RecordSet|NULL 
     */
    public function getChimpList($id)
    {
        $select = $this->select()
                ->where('list_id=?',$id);
        $resultSet = $this->fetchAll($select);
        if($resultSet->count()>0) {
            return $resultSet->current();
        }
        return NULL;
    }
    /**
     * getLists
     * 
     * gets all of the lists from the database
     * 
     * @return Zend_Db_Table_RowSet
     */
    public function getLists($listId=NULL)
    {
        try {
            $select = $this->select();
            if($listId!= NULL) {
                $select->where('list_id=?',$listId);
            }
            $resultSet = $this->fetchAll($select);
            if($resultSet->count()>0) {
                return $resultSet;
            }
            return NULL;
        } catch (Exception $ex) {
            $this->createTable();
            return $this->fetchAll();
        }
    }
    /**
     * createTable
     *
     * if the current table does not exists, create it
     */
    public function createTable()
    {
        try {
            $desc = $this->_db->describeTable($this->_name);
        } catch (exception $ex) {
            // table does not exist, create it
            $createSql = "CREATE TABLE `".$this->_name
                   ."` (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, "
                   ."list_id TEXT NOT NULL DEFAULT '', "
                   ."web_id INT NOT NULL, "
                   ."name VARCHAR(255) NOT NULL DEFAULT '', "
                   ."created_on DATE, "
                   ."alt TEXT NOT NULL DEFAULT '', "
                   ."notes TEXT NOT NULL DEFAULT '', "
                   ."params TEXT)";
            $this->_db->query($createSql);
            $indexSQL = "CREATE INDEX `id` on `".$this->_name."` (`id`)";
            $this->_db->query($indexSQL);
        }
    }
    /**
     * removeTable
     *
     * removes the table from the database
     */
    public function removeTable()
    {
        $dropSql = "DROP TABLE IF EXISTS `".$this->_name."`";
        $this->_db->query($dropSql);
    }
    /**
     * getSelectArray
     *
     * gets an array designed to be used in a select box
     * @return array of program names
     */
    public function getSelectArray() {
        // initialize the array
        $array = array();
        // create the model
        $mdl = $this;
        // get a select
        $select = $mdl->select();
        // add conditions
        $select->order("name asc");
        // get the results
        $results = $mdl->fetchAll($select);
        // loop through to create the array
        foreach($results as $result) {
            $array[$result->id] = $result->name;
        }
        return $array;
    }
    /**
     * createSelectBox
     *
     * creates a select box for inclusion in a form
     */
    public static function createSelectBox($name) {
        // create the model
        $mdl = new self();
        //create the element
        $element = new Zend_Form_Element_Select($name);
        $element->setLabel("List:")
                ->addMultiOption('',"(Select)")
                ->addMultiOptions($mdl->getSelectArray());
        return $element;
    }
    /**
     * addList
     *
     * adds a new banner to the database
     *
     * @param array $values
     * @return int
     */
    public function addList(array $values)
    {
        $row = $this->createRow();
        $row->created_on = date('Y-m-d');
        $id = $row->save();
        $this->updateList($id, $values);
        return $row->save();
    }
    /**
     * updateList
     *
     * updates a List in the database
     *
     * @param int $bannerId
     * @param array $values
     * @return int
     */
    public function updateList ($id, array $values)
    {
        $row = $this->getList($id);
        if($row==NULL) {
            throw new Exception("Error, could not find list to update:".$bannerId);
        }
        foreach($values as $name=>$val) {
            switch($name){
                case 'name':
                case 'list_id':
                case 'web_id':
                case 'alt':
                case 'params':
                case 'notes':
                    $row->$name=$val;
                    break;
                default:
                    break;
            }
        }
        return $row->save();
    }
    /**
     * deleteList
     * 
     * delets a list from the database
     * 
     * @param int $banner
     * @return type 
     */
    public function deleteList($id){
        $row = $this->getList($id);
        if($row!= NULL) {
            return $row->delete();
        }
        return NULL;
    }
}

