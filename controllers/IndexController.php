<?php
/**
 * JoomlaZend
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_HelloWorld
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_VALID_MOS') or
    die('Direct Access to this location is not allowed');
/**
 * IndexController
 *
 * Default controller
 *
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2010 Red Black Tree LLC
 * @category JoomlZend
 * @package ComZend_Chimpmail
 */
class Chimpmail_IndexController extends Core_Controller_Action
{
    /**
     * @var string|NULL api key
     */
    protected $_apiKey=NULL;
    /**
     * @var string|NULL api URL
     */
    protected $_apiUrl=NULL;
    /**
     * @var string|NULL source email
     */
    protected $_sourceEmail = NULL;
    /**
     * @var NULL or MCAPI the mail chimp api class
     */
    protected $_mcApi = NULL;
    /**
     * @var string the title for the current controller
     */
    protected $_title = "Mail Chimp Admin";
    /**
     * createMenu
     *
     * builds the menu options for this controller
     */
    public function createMenu()
    {
        try {
            if($this->_application->isAdmin()) {
                // top menu options
                JToolBarHelper::title($this->view->translate->_($this->_title),'ChimpMail');
                
                // main menu options
                $menuItems = array();
                $count = 0;
                // add menu Items
                // config menu item
                $menuItems[$count++] = array(
                    'link'=>JRoute::_($this->view->url(array(
                            'module'=>'Chimpmail',
                            'controller'=>'index',
                            'action'=>'config',
                        ),'administrator',true)),
                    'icon'=>'../../../components/'.ZEND_COMPONENT_NAME
                        .'/application/public/images/icons/48x48/configure.png',
                    'text'=>'Config',
                );
                // lists menu item
                $menuItems[$count++] = array(
                    'link'=>JRoute::_($this->view->url(array(
                            'module'=>'Chimpmail',
                            'controller'=>'lists',
                            'action'=>'adminindex',
                        ),'administrator',true)),
                    'icon'=>'header/icon-48-module.png',
                    'text'=>'Lists',
                );
                
                // send the menu items to the view
                $this->view->menuItems=$menuItems;
            }
        } catch (Exception $ex) {
            echo "Error creating menu:" . $ex->__toString();
        }
    }

    /**
     * init
     * 
     * initilizes the controller
     */
    public function init()
    {
        parent::init();
        if(Zend_Registry::isRegistered('chimpApiKey')) {
           $this->_apiKey = Zend_Registry::get('chimpApiKey');
           $this->_mcApi = new Chimp_MCAPI($this->_apiKey);
           //echo "apiKey:".$this->_apiKey."<br />";
        }
        if(Zend_Registry::isRegistered('chimpApiUrl')) {
           $this->_apiUrl = Zend_Registry::get('chimpApiUrl');
           //echo "apiUrl:".$this->_apiUrl."<br />";
        }
        if(Zend_Registry::isRegistered('chimpSourceEmail')) {
           $this->_sourceEmail = Zend_Registry::get('chimpSourceEmail');
           //echo "sourceEmail".$this->_sourceEmail."<br />";
        }

    }
    /**
     * indexAction
     *
     * Shows Hello World
     */
    public function indexAction()
    {
        $this->view->hello = $this->view->translate->_("Hello World");
    }
    /**
     * adminindexAction
     * 
     * shows Hello World
     */
    public function adminindexAction() 
    {
        if($this->_application->isAdmin()) {
            // create the menu 
            $this->createMenu();
//          LISTS
//            $lists = $this->_mcApi->lists();
//            if($this->_mcApi->errorCode) {
//                throw new Exception("Error loading lists:".$this->_mcApi->errorCode." ". $this->_mcApi->errorMessage);
//            } else {
//                echo "Lists that mached:".$lists['total']."<br />";
//                echo "Lists returned:".sizeof($lists['data'])."<br />";
//                foreach($lists['data'] as $list) {
//                    echo "Id:".$list['id'].'-'.$list['name']."<br />";
//                    echo "Web_id:".$list['web_id']."<br />";
//                    echo "Sub:".$list['stats']['member_count']."<br />";
//                    echo "Unsub:".$list['stats']['unsubscribe_count']."<br />";
//                    echo "Cleaned:".$list['stats']['cleaned_count']."<br />";
//                }
//            }
            
//          TEMPLATES
//            $types=array('user'=>true,'gallery'=>true,);
//            $templates = $this->_mcApi->templates($types);
//            if($this->_mcApi->errorCode) {
//                throw new Exception("Error loading templates:".$this->_mcApi->errorCode." ". $this->_mcApi->errorMessage);
//            } else {
//                echo "Your templates<br />";
//                foreach($templates['user'] as $tmpl) {
//                    echo $tmpl['id']."-".$tmpl['name']." ".$tmpl['layout']."<br />";
//                }
//                echo "Gallery templates<br />";
//                foreach($templates['gallery'] as $tmpl) {
//                    echo $tmpl['id']."-".$tmpl['name']." ".$tmpl['layout']."<br />";
//                }
//            }
        }
    }
    /**
     * configAction
     * 
     * shows the configure form
     */
    public function configAction()
    {
        if($this->_getParam('Cancel')!=NULL||$this->_getParam('Cancel_x')!=NULL) {
            $this->_redirect($this->view->Jurl(array(
                    'module'=>$this->_module,
                    'controller'=>'index',
                    'action'=>'adminindex',
                ),'administrator',false));
        } 
        if($this->_application->isAdmin()) {
            // top menu options
            JToolBarHelper::title($this->view->translate->_($this->_title." Configure"));
            $frm = new Chimpmail_Form_Config();
            if($this->getRequest()->isPost()) {
                if($frm->isValid($_POST)) {
                    // save the values
                    $mdlModule = new Model_ZFModules();
                    $module = $mdlModule->getModuleByName("/Chimpmail");
                    $module->params = json_encode($frm->getValues());
                    // success
                    $this->_redirect($this->view->Jurl(array(
                        'module'=>$this->_module,
                        'controller'=>'index',
                        'action'=>'adminindex',
                    ),'administrator',false));
                }
            } else {
                // populate the form
                if($this->_apiKey!= NULL) {$frm->getElement('apiKey')->setValue($this->_apiKey);}
                if($this->_apiUrl!= NULL) {$frm->getElement('apiUrl')->setValue($this->_apiUrl);}
                if($this->_sourceEmail!= NULL) {$frm->getElement('sourceEmail')->setValue($this->_sourceEmail);}
            }
            
            $this->view->form = $frm;
        }
    }
}